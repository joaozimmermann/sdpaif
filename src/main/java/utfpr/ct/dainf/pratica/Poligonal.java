package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    private final List<T> vertices = new ArrayList<>();

    public int getN() {
        return vertices.size();
    }

    public List<T> getVertices() {
        return vertices;
    }
    
    
    public T get(int i) throws RuntimeException{
        if (vertices.size()<i)
            throw new RuntimeException(String.format("get(%d): índice inválido", i));
        else
            return vertices.get(i);
        
    }
    public void set(int i, T p) throws RuntimeException{
        if (i>vertices.size()|| i<0)
            throw new RuntimeException(String.format("set(%d): índice inválido", i));
        else
            vertices.add(i, p);
    }
    public double getComprimento(){
        Ponto2D res[];
        int i=0;
        res = new Ponto2D[vertices.size()];
        double res2 = 0;
        for(Ponto2D a: vertices){
//            res[i] = Math.sqrt(Math.pow(a.getX(), 2) + Math.pow(a.getY(), 2) + Math.pow(a.getZ(), 2));
            res[i].setX(a.getX());
            res[i].setY(a.getX());
            res[i].setZ(a.getX());
        }
        for (int j=0; j<i-1; j++)
            res2 += Math.sqrt(Math.pow((res[j].getX()-res[j+1].getX()), 2) + 
                    Math.pow((res[j].getY()-res[j+1].getY()), 2) + 
                    Math.pow((res[j].getZ()-res[j+1].getZ()), 2));
        
        return res2;
    }
}
